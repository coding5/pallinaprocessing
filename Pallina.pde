final int WIDTH = 1300, HEIGHT = 600;
final int radius = 30;
final float shadowWeigth = 20;

float rho = 10, theta = PI/5;
float frict = 1, frictStep = 0.01;
float[] x = new float[10];
float[] y = new float[10];

int r = 255, g = 255, b = 255;

void setup(){
  size(1300, 600);
  for(int i = 0; i < 9; i++){
    x[i] = 400;
    y[i] = 100;
  }
}

void draw(){
  background(22);
  if(x[0] <= radius/2 || x[0] >= WIDTH-radius/2 ) {
    theta = PI - theta;
  }
  if(y[0] <= radius/2 || y[0] >= HEIGHT-radius/2) {
    theta = 2*PI - theta;
  }
  x[0] += rho*cos(theta);
  y[0] += rho*sin(theta);
  rho *= frict;
  
  noStroke();
  for(int i = x.length-1; i > 0; i--){
    x[i] = x[i-1];
    y[i] = y[i-1];
    fill(r, g, b, (x.length - i)*shadowWeigth);
    circle(x[i], y[i], radius);
  }
  
  stroke(2);
  circle(x[0], y[0], radius);
}

void mouseClicked(){
  frict = 1;
  r = 255;
  g = 255;
  b = 255;
}

void mousePressed(){
  if(mouseButton == LEFT){
    r = 0;
    g = 0;
    b = 255;
    frict += frictStep;
  }
  else if(mouseButton == RIGHT){
    r = 255;
    g = 0;
    b = 0;
    frict -= frictStep;
  } 
}
